<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content_add';
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
