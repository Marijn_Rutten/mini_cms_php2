<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'indexController@index');

Route::get('auth/register','Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');



Route::get('content/add', [
    'middleware' => 'auth',
    'uses' => 'addContentController@create'
]);

Route::get('/content/{id}', 'indexController@show');

Route::get('content/add', 'addContentController@create');

Route::post('content/add','addContentController@store');

Route::post('/comment', 'indexController@store');

Route::get('content', [
    'middleware' => 'auth',
    'uses' => 'addContentController@index'
]);


//Route::get('/register', 'Auth\AuthController@getRegister');
//Route::post('/register', 'Auth\AuthController@postRegister');