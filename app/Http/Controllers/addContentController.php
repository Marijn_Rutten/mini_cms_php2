<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DOMNode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;

use PHPHtmlParser\Dom;
use Goutte\Client;
include("simple_html_dom.php");


class addContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = DB::table('content_add')
            ->select('*')
            ->where('user_id', '=', Auth::id())
            ->orderBy('created_at', 'desc')
            ->get();

        return view('content', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addContent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'url' => 'required|max:255',
        ]);

        $input_url = $request->input('url');

            if (strpos($input_url,'youtube.com') !== false) {
                $type = 'youtube';

                $doc = new \DOMDocument();
                $doc->loadHTML(file_get_contents($input_url));
                $title = $doc->getElementsByTagName("title")->item(0)->nodeValue;

                $link_id = substr($input_url, strpos($input_url, "watch?v=") + 8);
                DB::table('content_add')->insert([
                    [
                        'url' => $input_url,
                        'title' => $title,
                        'link_id'=> $link_id,
                        'type' => $type,
                        'user_id'=> Auth::id(),
                        'created_at' => Carbon::now(),
                    ],
                ]);
                $info = "Youtube added!";
            }else if(strpos($input_url,'vimeo.com') !== false) {
                $type = 'vimeo';

                $doc = new \DOMDocument();
                libxml_use_internal_errors(true);
                $doc->loadHTML(file_get_contents($input_url));
                $title = $doc->getElementsByTagName("title")->item(0)->nodeValue;

                $link_id = substr($input_url, strpos($input_url, "vimeo.com/") + 10);
                DB::table('content_add')->insert([
                    [
                        'url' => $input_url,
                        'title' => $title,
                        'link_id' => $link_id,
                        'type' => $type,
                        'user_id' => Auth::id(),
                        'created_at' => Carbon::now(),
                    ],
                ]);
                $info = "Vimeo added!";
            } else if(strpos($input_url,'soundcloud.com') !== false){
                $type = 'soundcloud';

                $doc = new \DOMDocument();

                $options = array(
                    'http' => array(
                        'ignore_errors' => true,
                        'user_agent' => $_SERVER['HTTP_USER_AGENT']
                    )
                );

                $context = stream_context_create($options);

                @$doc->loadHTML(file_get_contents($input_url, false, $context));
                $title = $doc->getElementsByTagName("title")->item(0)->nodeValue;

                $link_id = '';
                $startsWith = 'soundcloud://sounds:';

                foreach ($doc->getElementsByTagName("meta") as $meta) {
                    if ($meta->hasAttribute('property') && $meta->getAttribute('property') === 'twitter:app:url:googleplay') {
                        $link_id = $meta->getAttribute('content');
                        if (substr($link_id, 0, strlen($startsWith))) {
                            $link_id = substr($link_id, strlen($startsWith), strlen($link_id));
                            break;
                        }
                    }
                }


                /*$tags = get_meta_tags($input_url);
                $link_id = $tags['content'];*/
                DB::table('content_add')->insert([
                    [
                        'url' => $input_url,
                        'title' => $title,
                        'link_id'=> $link_id,
                        'type' => $type,
                        'user_id'=> Auth::id(),
                        'created_at' => Carbon::now(),
                    ],
                ]);
                $info = "soundcloud added!";
            }else{
                $type = 'Other';
                $doc = new \DOMDocument();
                libxml_use_internal_errors(true);
                $doc->loadHTML(file_get_contents($input_url));
                $title = $doc->getElementsByTagName("title")->item(0)->nodeValue;

                $img = $doc->getElementsByTagName("img")->item(0)->getAttribute('src');;
                $link_id = $img;

                DB::table('content_add')->insert([
                    [
                        'url' => $input_url,
                        'title' => $title,
                        'link_id'=> $input_url."/".$link_id,
                        'type' => $type,
                        'user_id'=> Auth::id(),
                        'created_at' => Carbon::now(),
                    ],
                ]);
                $info = "Link added";
            }
        return view('addContent', compact('info'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
