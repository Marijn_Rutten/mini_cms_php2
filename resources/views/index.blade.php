@extends("layouts.master")

@section("content")

    <h1>Mini cms content</h1>
    @if(isset($items))
    @foreach($items as $item)
        @if($item->type == "youtube")

            <a href="/content/{{ $item->id }}"><h2>{{$item->title}}</h2></a>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$item->link_id}}" frameborder="0" allowfullscreen></iframe>
        @elseif($item->type == "vimeo")

            <a href="/content/{{ $item->id }}"><h2>{{$item->title}}</h2></a>
            <iframe width="560" height="315" src="//player.vimeo.com/video/{{$item->link_id}}" frameborder="0" allowfullscreen></iframe>
        @elseif($item->type == "soundcloud")

            <a href="/content/{{ $item->id }}"><h2>{{$item->title}}</h2></a>
            <iframe width="560" height="315" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{$item->link_id}}&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
        @else

            <a href="/content/{{ $item->id }}"><h2>{{$item->title}}</h2></a>
            <img width="560" height="315" src="{{$item->link_id}}" alt="">
        @endif
    @endforeach
    @endif
@stop