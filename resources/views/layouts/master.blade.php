<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>
    <link href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/bootstrap/js/bootstrap.min.js') }}" rel="stylesheet">
</head>
<body>
<div class="container">
    @include('layouts.nav')

    @yield('content')
</div>


</body>
</html>