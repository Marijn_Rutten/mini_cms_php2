<nav>
    <ul class="nav nav-pills">
        <li><a href="/">Home</a></li>
        <li><a href="/content">Content</a></li>
        @if(Auth::check())
            <li><a href="/auth/logout">Logout</a></li>
        @else
            <li><a href="/auth/login">Login</a></li>
            <li><a href="/auth/register">Register</a></li>
        @endif
    </ul>
</nav>