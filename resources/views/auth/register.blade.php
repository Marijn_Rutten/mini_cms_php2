@extends("layouts.master")

@section("content")
<h1>register</h1>
<form method="POST" action="register">
    @if(isset($info))
    <li class="form-group has-error">{{ $info }}</li>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="form-group has-error">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div>
        Name
        <input type="text" name="name" value="{{ old('name') }}">
    </div>

    <div>
        Email
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        Password
        <input type="password" name="password">
    </div>

    <div>
        Confirm Password
        <input type="password" name="password_confirmation">
    </div>

    <div>
        Login code
        <input type="text" name="code" value="{{ old('code') }}">
    </div>

    <div>
        <button type="submit">Register</button>
    </div>
</form>
@stop