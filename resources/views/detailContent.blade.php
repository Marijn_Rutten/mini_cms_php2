@extends("layouts.master")

@section("content")
    <h2>{{$content->title}}</h2>
@if($content->type == "youtube")
    <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$content->link_id}}" frameborder="0" allowfullscreen></iframe>
@elseif($content->type == "vimeo")
    <iframe width="560" height="315" src="//player.vimeo.com/video/{{$content->link_id}}" frameborder="0" allowfullscreen></iframe>
@elseif($content->type == "soundcloud")
    <iframe width="560" height="315" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{{$content->link_id}}&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
@else
    <img width="560" height="315" src="{{$content->link_id}}" alt="">
@endif
    <form method="POST" id="comment-form" action="/comment">
        {!! csrf_field() !!}
        <textarea name="body" rows="3" placeholder="comment here..."></textarea>
        <input type="hidden" name="content_id" value="{{ $content->id }}">
        <button type="submit">Comment</button>
    </form>

    @if(isset($comments))
        @foreach($comments as $comment)
            <p><b>{{$comment->name}}</b></p>
            <p>{{$comment->body}}</p>
        @endforeach
    @endif

@stop