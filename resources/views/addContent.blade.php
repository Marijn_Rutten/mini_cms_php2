@extends("layouts.master")

@section("content")
<h1>Add content</h1>

<form method="POST" class="form-horizontal" action="/content/add">
    {!! csrf_field() !!}
    @if(isset($info))
        {{var_dump($info)}}
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="form-group has-error">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div>
        URL
        <input type="text" name="url" placeholder="http://..." value="{{ old('url') }}">
    </div>

    <div>
        <button type="submit">Submit</button>
    </div>
</form>
@stop