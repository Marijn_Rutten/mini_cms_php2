# mini cms opdracht voor php

##using the app (after vagrant up)
Surf to 192.168.33.10 or make your own alias in hosts file

##URL to repository
[link to repository](https://Marijn_Rutten@bitbucket.org/Marijn_Rutten/mini_cms_php2.git)

https://Marijn_Rutten@bitbucket.org/Marijn_Rutten/mini_cms_php2.git

##secret register code
s4nt4

#### Start or resume your server
```bash
vagrant up
```
#### SSH into your server
```bash
vagrant ssh
```

- Hostname: localhost or 127.0.0.1
- Username: root
- Password: root
- Database: scotchbox

#### SSH Access

- Hostname: 127.0.0.1:2222
- Username: vagrant
- Password: vagrant
